import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';

import './styles/global.css';
import 'react-grid-layout/css/styles.css';
import AppRoutes from './routes';

function App() {
  return (
    <>
      <Router>
        <AppRoutes />
      </Router>
    </>
  );
}

export default App;

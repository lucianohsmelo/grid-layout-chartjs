import React, { useState, useEffect } from 'react';
import { Bar } from 'react-chartjs-2';

const getRandomArray = (numItems: number) => {
    let names = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    let data = [];
    for (var i = 0; i < numItems; i++) {
        data.push({
            label: names[i],
            value: Math.round(20 + 80 * Math.random())
        });
    }
    return data;
}

const BarChart: React.FC = () => {
    const [data, setData] = useState<{}>({});

    useEffect(() => {
        const randomData = getRandomArray(26);
        setData({
            labels: randomData.map(d => d.label),
            datasets: [{
                data: randomData.map(d => d.value),
                backgroundColor: 'rgba(75,192,192,1)',
            }]
        })
    }, []);

    return (
        <Bar
            data={data}
            width={100}
            height={50}
            options={{
                maintainAspectRatio: false,
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [
                        {
                            ticks: {
                                min: 0,
                                max: 100
                            }
                        }
                    ]
                }
            }}
        />
    );
}

export default BarChart;
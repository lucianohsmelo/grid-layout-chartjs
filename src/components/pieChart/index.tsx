import React, { useState, useEffect } from 'react';
import { Pie } from 'react-chartjs-2';

const Chart: React.FC = () => {
  const [data, setData] = useState<{}>({});

  useEffect(() => {
    setData({
      labels: ['January', 'February', 'March',
        'April', 'May'],
      datasets: [
        {
          label: 'Rainfall',
          backgroundColor: [
            '#B21F00',
            '#C9DE00',
            '#2FDE00',
            '#00A6B4',
            '#6800B4'
          ],
          hoverBackgroundColor: [
            '#501800',
            '#4B5000',
            '#175000',
            '#003350',
            '#35014F'
          ],
          data: [65, 59, 80, 81, 56]
        }
      ]
    })
  }, []);

  return (
    <Pie
      data={data}
      options={{
        maintainAspectRatio: false,
        title: {
          display: false,
        },
        legend: {
          display: true,
          position: 'right'
        }
      }}
    />
  );
}

export default Chart;
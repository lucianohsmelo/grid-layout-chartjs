import React from 'react';
import RGL, { WidthProvider } from "react-grid-layout";

import BarChart from '../../components/barChart';
import DoughnutChart from '../../components/doughnutChart';
import PieChart from '../../components/pieChart';

const ReactGridLayout = WidthProvider(RGL);

const onLayoutChange = (layout: RGL.Layout[]) => {
    console.log(layout);
}

const Home: React.FC = () => {
    return (
        <ReactGridLayout onLayoutChange={onLayoutChange}>
            <div key="a" data-grid={{ w: 6, h: 2, x: 0, y: 0 }}>
                <BarChart />
            </div>
            <div key="b" data-grid={{ w: 6, h: 2, x: 6, y: 0 }}>
                <DoughnutChart />
            </div>
            <div key="c" data-grid={{ w: 6, h: 2, x: 6, y: 2 }}>
                <PieChart />
            </div>
        </ReactGridLayout>
    );
}

export default Home;
import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Home from './pages/home';

const Routes = () => (
    <div className="view-routes">
        <Switch>
            <Route path="/" exact={true} component={Home} />
        </Switch>
    </div>
);

export default Routes;